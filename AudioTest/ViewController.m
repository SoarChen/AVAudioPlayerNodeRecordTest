//
//  ViewController.m
//  AudioTest
//
//  Created by Soar Chen on 2017/7/13.
//  Copyright © 2017年 Soar. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController ()
@property (nonatomic, strong) AVAudioPlayerNode *internalPlayer;
@property (nonatomic, strong) AVAudioMixerNode *mixNode;
@property (nonatomic, strong) AVAudioEngine *engine;
@property (nonatomic, assign) BOOL isPlaying;
@property (nonatomic, assign) BOOL isRecording;
@property (nonatomic, strong) AVAudioPCMBuffer *audioFileBuffer;
@property (nonatomic, strong) AVAudioFile *internalAudioFile;
@property (nonatomic, assign) NSUInteger framesToPlayCount;
@property (nonatomic, assign) NSUInteger totalFrameCount;
@property (nonatomic, strong) AVAudioFile *recordAudioFile;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self initAudio];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)initAudio {
//    AVAudioSession *session = [AVAudioSession sharedInstance];
//    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
//    [session setPreferredIOBufferDuration:512 error:nil];
//    [session setActive:YES error:nil];
    _engine = [[AVAudioEngine alloc] init];
    _internalPlayer = [[AVAudioPlayerNode alloc] init];
    [_engine attachNode:_internalPlayer];
    
    _isPlaying = NO;
    NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:@"piano" withExtension:@"m4a"];
    _internalAudioFile = [[AVAudioFile alloc] initForReading:fileUrl error:nil];
    _totalFrameCount = _internalAudioFile.length;
    _audioFileBuffer = [[AVAudioPCMBuffer alloc] initWithPCMFormat:_internalAudioFile.processingFormat frameCapacity:(AVAudioFrameCount)_totalFrameCount];
    [_internalAudioFile readIntoBuffer:_audioFileBuffer error:nil];
    
    
    // Connect Nodes
    _mixNode = [[AVAudioMixerNode alloc] init];
    [self.engine attachNode:_mixNode];
    [self.engine connect:_internalPlayer
                      to:_mixNode
                  format:_internalAudioFile.processingFormat];
    
    AVAudioMixerNode *mainMix = _engine.mainMixerNode;
    [self.engine connect:_mixNode to:mainMix format:_internalAudioFile.processingFormat];
    NSString *path = NSTemporaryDirectory();
    NSString *tempPath = [path stringByAppendingPathComponent:@"tmp_record.caf"];
    NSURL *url = [NSURL fileURLWithPath:tempPath];
    
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    [recordSetting setValue:[NSNumber numberWithInt:1] forKey:AVLinearPCMIsNonInterleaved];
    [recordSetting setValue:[NSNumber numberWithInt:32] forKey:AVLinearPCMBitDepthKey];
    [recordSetting setValue:[NSNumber numberWithInt:1] forKey:AVLinearPCMIsFloatKey];
    [recordSetting setValue:[NSNumber numberWithInt:0] forKey:AVLinearPCMIsBigEndianKey];
    
    //AVAudioFormat *format = [[AVAudioFormat alloc] initStandardFormatWithSampleRate:44100 channels:2];
    AVAudioFormat *format = [[AVAudioFormat alloc] initWithSettings:recordSetting];
    NSError *error;
    _recordAudioFile = [[AVAudioFile alloc] initForWriting:url settings:recordSetting error:&error];
    [_engine startAndReturnError:nil];
}
- (IBAction)clickPlayBtn:(UIButton *)sender {
    [self play];
}


- (void) play {
    if (_isPlaying) {
        return;
    }
    
    _isPlaying = YES;
    
    //[_internalPlayer play];
    [_internalPlayer scheduleBuffer:_audioFileBuffer
                                  atTime:nil
                                 options:AVAudioPlayerNodeBufferInterrupts
                       completionHandler:nil];
     [_internalPlayer play];
}

- (void) record {
    if (_isRecording) {
        return;
    }
    
    _isRecording = YES;
    AVAudioFrameCount recordingBufferLength = 8192;
    //[_internalPlayer scheduleFile:_internalAudioFile atTime:nil completionHandler:nil];
    AVAudioFormat *format = [_mixNode outputFormatForBus:0];
    [_mixNode installTapOnBus:0 bufferSize:recordingBufferLength format:format block:^(AVAudioPCMBuffer *buff, AVAudioTime *when) {
        //[_recordAudioFile writeFromBuffer:buffer error:nil];
        
        //[_internalPlayer scheduleBuffer:buff completionHandler:nil];
        NSError *error;
        [_recordAudioFile writeFromBuffer:buff error:&error];
       NSLog(@"Soar test when = %@", when);
    }];
    
    
}
- (IBAction)clickRecord:(UIButton *)sender {
    [self record];
}
- (IBAction)clickStopRecord:(UIButton *)sender {
    [_mixNode removeTapOnBus:0];
    [_internalPlayer stop];
    
}


@end
